package suresh.com.mvp;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements MvpInterface {
    EditText userName;
    EditText userPassword;
    ImplematationClass implematationClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        userName = (EditText) findViewById(R.id.userNmae);
        userPassword = (EditText) findViewById(R.id.userPassword);
        implematationClass = new ImplematationClass(this);
    }

    public void click(View view) {
        implematationClass.onEmptyVlidation(userName.getText().toString().trim(),userPassword.getText().toString().trim());
    }

    @Override
    public void onFieldValidation() {
        Toast.makeText(MainActivity.this,"FieldisEmpty",Toast.LENGTH_SHORT).show();
    }



    @Override
    public void onSuccess() {
        Toast.makeText(MainActivity.this,"OnSuccess",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError() {
        Toast.makeText(MainActivity.this,"OnError",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUserNameError(String command) {
        userName.setError(command);
    }
}
