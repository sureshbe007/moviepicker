package suresh.com.mvp;

/**
 * Created by Lenovo on 8/29/2016.
 */
public interface MvpInterface {

    public void onFieldValidation();

    public void onSuccess();

    public void onError();

    public void onUserNameError(String command);


}
